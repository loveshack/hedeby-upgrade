package com.sun.grid.grm.upgrade.release10u3;

/*___INFO__MARK_BEGIN__*/

/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2009 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.TestUtil;
import com.sun.grid.grm.bootstrap.DummyExecutionEnvFactory;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.bootstrap.PathUtil;
import com.sun.grid.grm.util.FileUtil;
import com.sun.grid.grm.util.Platform;
import java.io.File;
import java.util.logging.Logger;
import junit.framework.TestCase;

/**
 *  Test the upgrade of the java policy file
 */
public class UpgradeJavaPolicyFileTest extends TestCase {

    private final static Logger log = Logger.getLogger(UpgradeJavaPolicyFileTest.class.getName());
    private ExecutionEnv env;
    private File policyFile;
    private File securityDir;
    private File policyFileTemplate;
    
    public UpgradeJavaPolicyFileTest(String testName) {
        super(testName);
    }

    @Override
    protected void setUp() throws Exception {
        env = DummyExecutionEnvFactory.newInstance();
        policyFile = PathUtil.getSecurityJavaPolicy(env);
        securityDir = policyFile.getParentFile();
        if (!securityDir.mkdirs()) {
            throw new Exception("Could not create directory " + securityDir);
        }
        policyFileTemplate = new File(PathUtil.getTemplatePath(env), "java.policy.template");
        if (!policyFileTemplate.getParentFile().exists() && !policyFileTemplate.getParentFile().mkdirs()) {
            throw new Exception("Could not create directory " + policyFileTemplate.getParentFile());
        }
    }

    @Override
    protected void tearDown() throws Exception {
        Platform.getPlatform().removeDir(env.getLocalSpoolDir(), true);
        Platform.getPlatform().removeDir(env.getDistDir(), true);
    }

    public void testUpgradeWithSuccess() throws Exception {

        String orgContent = "java policy 1.0u3";
        String newContent = "java policy 1.0u5";

        FileUtil.write(orgContent, policyFile);
        FileUtil.write(newContent, policyFileTemplate);

        UpgradeJavaPolicyFile upgrade = new UpgradeJavaPolicyFile();
        upgrade.execute(env);
        assertEquals("Java policy file has invalid content", newContent, FileUtil.read(policyFile));

        File backupFile = new File(policyFile.getParentFile(), policyFile.getName() + ".10u3");
        assertTrue("Backup of java policy file must exist (" + backupFile + ")", backupFile.exists());
        assertEquals("Backup of java policy file has invalid content", orgContent, FileUtil.read(backupFile));
    }

    public void testUndoAfterSuccessfulUpgrade() throws Exception {
        String orgContent = "java policy 1.0u3";
        String newContent = "java policy 1.0u5";

        FileUtil.write(orgContent, policyFile);
        FileUtil.write(newContent, policyFileTemplate);

        UpgradeJavaPolicyFile upgrade = new UpgradeJavaPolicyFile();

        upgrade.execute(env);

        upgrade.undo(env);
        File backupFile = new File(policyFile.getParentFile(), policyFile.getName() + ".10u3");
        assertEquals("Java policy file has invalid content", orgContent, FileUtil.read(policyFile));
        assertFalse("Backup of policy file must not exist (" + backupFile + ")", backupFile.exists());
    }

    public void testUpgradePolicyFileDoesNotExist() throws Exception {
        UpgradeJavaPolicyFile upgrade = new UpgradeJavaPolicyFile();
        try {
            upgrade.execute(env);
            fail("Upgrade of java policy file must fail if the java policy file does not exist");
        } catch(GrmException ex) {
            TestUtil.assertMatchingError("UpgradeJavaPolicyFile.ex.noPolicyFile", UpgradeJavaPolicyFile.BUNDLE, ex);
        }
    }

}
