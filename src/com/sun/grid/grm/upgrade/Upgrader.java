/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2009 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/


package com.sun.grid.grm.upgrade;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.bootstrap.UpgradeResult;
import java.util.List;
import com.sun.grid.grm.ui.Command;
import com.sun.grid.grm.upgrade.release10u3.BootstrapVersioningCommand;
import com.sun.grid.grm.upgrade.release10u3.RemoveCloudAdapter;
import com.sun.grid.grm.upgrade.release10u3.ResetCSFilesCommand;
import com.sun.grid.grm.upgrade.release10u3.UpgradeJavaPolicyFile;
import com.sun.grid.grm.upgrade.release10u3.UpgradePreferences;
import com.sun.grid.grm.upgrade.release10u3.UpgradeReporter;
import com.sun.grid.grm.upgrade.release10u3.UpgradeResourceFiles;
import com.sun.grid.grm.upgrade.release10u3.UpgradeSecurity;
import com.sun.grid.grm.util.Hostname;
import java.util.LinkedList;
import java.util.Stack;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;

/**
 * Upgrader is responsible for generating proper command list for
 * a given upgrade. Executing those commands and reverting changes in case of error.
 */
public class Upgrader {

    private final ExecutionEnv env;
    //Currently not used. For future release we need this info to distinguish if we upgrade from 1.0u3 or 1.0u5
    private final String hostVersion;
    private final String binaryVersion;
    private final List<Command> cmds = new LinkedList<Command>();
    private final Stack<Command> undocmds = new Stack<Command>();
    private final Stack<Command> postUpgradeCmds = new Stack<Command>();
    private static final String BUNDLE = "com.sun.grid.grm.upgrade.messages";
    private final Logger log = Logger.getLogger(Upgrader.class.getName(), BUNDLE);

    /**
     * Cunstructor for a given Upgrader
     * @param env ExecutionEnv
     * @param hostVersion current version of the host
     * @param binaryVersion - version of the binaries
     */
    public Upgrader(ExecutionEnv env, String hostVersion, String binaryVersion) {
        this.env = env;
        this.hostVersion = hostVersion;
        this.binaryVersion = binaryVersion;
        //retrieve upgrade commands to execute
        //only supported upgrade so far is 1.0u3 so simply add commands
        cmds.add(new TriggerUpgrade(hostVersion, binaryVersion));
        cmds.add(new UpgradePreferences());
        BootstrapVersioningCommand bvCmd = new BootstrapVersioningCommand();
        bvCmd.setVersion(binaryVersion);
        cmds.add(bvCmd);
        cmds.add(new RemoveCloudAdapter());
        cmds.add(new UpgradeResourceFiles());
        cmds.add(new UpgradeReporter());
        cmds.add(new UpgradeSecurity());
        cmds.add(new UpgradeJavaPolicyFile());
        //Prepare cleanup for upgrade for 1.0u3
        if (Hostname.getLocalHost().equals(env.getCSHost())) {
            postUpgradeCmds.push(new ResetCSFilesCommand());
        }
        

    }
    /**
     * Upgrade executions could have some general checks before real upgrade can be triggered.
     * Checks like: if some files are available and so on.
     */
    public void checkPrerequisites() {
        //No checks for 1.0u3

    }
    
    /**
     * Trigger execution of upgrade
     * @return UpgradeResult
     */
    public UpgradeResult execute() {
        UpgradeResult result = new UpgradeResult();
        UpgradeStep current;
        try {
            for (Command cmd : cmds) {
                current = (UpgradeStep) cmd;
                try {
                    cmd.execute(env);
                    undocmds.push(cmd);
                    result.addSuccessfulStep(current.getName());
                } catch (Throwable ex) {
                    //Issue 701 - if runtime exception we are not sure that undo was executed for current command
                    cmd.undo(env);
                    this.undo();
                    if (ex instanceof GrmException) {
                        result.setError((GrmException)ex, current.getName());
                    } else {
                        result.setError(new GrmException("Upgrader.error.unexpected", ex, BUNDLE, getErrorMessage(ex)), current.getName());
                    }
                    return result;
                }
            }
        } finally {
            cleanup();
        }
        return result;
    }
    /**
     * Undo all executed successfully commands
     */
    private void undo() {
        //undo the already executed commands
        while (!undocmds.empty()) {
            undocmds.pop().undo(env);
        }
    }

    /**
     * Post upgrade cleanup operations, executed when upgrade finished with error or success
     */
    private void cleanup() {
        //undo the already executed commands
        while (!postUpgradeCmds.empty()) {
            try {
                postUpgradeCmds.pop().execute(env);
            } catch (GrmException ex) {
                LogRecord lr = new LogRecord(Level.WARNING, "Upgrader.post.error");
                lr.setParameters(new Object[]{ex.getLocalizedMessage()});
                lr.setThrown(ex);
                log.log(lr);
            }
        }
    }
    /**
     * Utility method for abtaining meaningful info for user from Throwable
     * @param ex Throwable
     * @return String representing error message.
     */
    private static String getErrorMessage(Throwable ex) {
        if (ex.getLocalizedMessage() != null) {
            return ex.getLocalizedMessage();
        }
        if (ex.getMessage() != null) {
            return ex.getMessage();
        }
        return ex.toString();
    }
}
