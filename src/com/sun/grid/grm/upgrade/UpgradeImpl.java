/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2009 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/


package com.sun.grid.grm.upgrade;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.bootstrap.Upgrade;
import com.sun.grid.grm.bootstrap.UpgradeResult;
import com.sun.grid.grm.util.Hostname;

/**
 * Utility class for whole upgrade sub-module.
 */

public class UpgradeImpl implements Upgrade{

    private final static String binaryVersion = "1.0u5";
    private final static String[] supportedVersions = {"1.0u3"};
    private final static String BUNDLE = "com.sun.grid.grm.upgrade.messages";

    /**
     * Get binaryVersion of binaries
     * @return string representing version of binaries
     */
    public String getBinaryVersion() {
        return binaryVersion;
    }

    /**
     * This is method creates Upgrader that is responsible for execution of upgrade
     * @param env ExecutionEnv
     * @param oldVersion - current version of host
     * @return String representing version to which host was upgraded
     * @throws GrmException - if upgrade fails
     */
    public UpgradeResult upgradeHost(ExecutionEnv env, String hostVersion, String systemVersion) throws GrmException {
        //logic to upgrade host

        if (isUpgradeNecessary(env, hostVersion, systemVersion)) {
            Upgrader upgrade = new Upgrader(env, hostVersion, getBinaryVersion());
            upgrade.checkPrerequisites();
            return upgrade.execute();
        }
        return new UpgradeResult(true);

    }

    /**
     * Check version of the host is supported by this binary. And if host should be upgraded.
     * @param hostVersion - version of the current host
     * @param systemVersion -version of system
     * @return true if version of current host should be upgraded.
     * @throws com.sun.grid.grm.GrmException - if binaryVersion is unsupported
     */
    public boolean isUpgradeNecessary(ExecutionEnv env, String hostVersion, String systemVersion)  throws GrmException {
        

        //Ignore this check on master host as hostVersion is equal systemVersion
        if (!Hostname.getLocalHost().equals(env.getCSHost()) && !binaryVersion.equals(systemVersion)) {
            throw new GrmException("UpgradeImpl.error.wrongsystemversion", BUNDLE, binaryVersion, systemVersion);
        }

        if (binaryVersion.equals(hostVersion)) {
            return false;
        }
        //check if we support version
        for (int i=0; i<supportedVersions.length; i++) {
            if (supportedVersions[i].equals(hostVersion)) {
                return true;

            }
        }
      throw new GrmException("UpgradeImpl.error.unsupportedversion", BUNDLE, hostVersion, binaryVersion);
        
    }

}
