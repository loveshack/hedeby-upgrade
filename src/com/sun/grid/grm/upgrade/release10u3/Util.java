/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2009 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/


package com.sun.grid.grm.upgrade.release10u3;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.bootstrap.PathUtil;
import com.sun.grid.grm.config.common.Component;
import com.sun.grid.grm.config.common.GlobalConfig;
import com.sun.grid.grm.config.common.JvmConfig;
import com.sun.grid.grm.config.common.Singleton;
import com.sun.grid.grm.util.Hostname;
import com.sun.grid.grm.util.Platform;
import java.io.File;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

/**
 * Utility class for upgrade on 1.0u3 system
 * 
 */
public class Util {

    /**
     * BUNDLE name for all messages in package
     */
    private static final String BUNDLE="com.sun.grid.grm.upgrade.release10u3.messages";
    /**
     * Utility method for identifying who should be the owner of files for Sigleton components on current host
     * @param global GlobalConfig configuration
     * @return list of ComponentOwner objects contaning information about components that are
     * Singleton type and are running on current host (localhost)
     */
    public static List<ComponentOwner> getSingletonComponentsWithOwners(GlobalConfig global) {

        List<ComponentOwner> ret = new LinkedList<ComponentOwner>();
        for (JvmConfig j : global.getJvm()) {
            for (Component c : j.getComponent()) {
                if (c instanceof Singleton) {
                        if (Hostname.getLocalHost().equals(Hostname.getInstance(((Singleton)c).getHost()))) {
                            ret.add(new ComponentOwner(c.getName(),j.getUser(),c.getClassname()));
                        }
                }
            }
        }
        return ret;
    }
    /**
     * Retrieve the owner of specific component from global configuration
     * @param global - GlobalConfig from cs
     * @param componentName - name of component
     * @return String representing user that owns the component
     * @throws com.sun.grid.grm.GrmException when component cannot be found
     */
    public static String getComponentOwner (GlobalConfig global, String componentName) throws GrmException{
        for (JvmConfig j : global.getJvm()) {
            for (Component c : j.getComponent()) {
                if (c.getName().equals(componentName)) {
                    return j.getUser();
                }
            }
        }
        throw new GrmException("Util.error.componentnotfound", BUNDLE, componentName);
    }
    /**
     * Set proper owner on directories/files spooled by component
     * @param env ExecutionEnv
     * @param componentName name of component
     * @param owner - user that should own the directories/files
     * @throws com.sun.grid.grm.GrmException on errors while changing owner
     */
    public static void resetComponentFiles(ExecutionEnv env, String componentName, String owner) throws GrmException{
        File location = PathUtil.getSpoolDirForComponent(env, componentName);
        try {            
            Platform.getPlatform().chown(location, owner, true);
        } catch (IOException ex) {
            throw new GrmException("Util.error.changeowner", BUNDLE, ex,location.getAbsolutePath(), owner);
        } catch (InterruptedException ex) {
             throw new GrmException("Util.error.changeowner", BUNDLE, ex,location.getAbsolutePath(), owner);
        }
    }
}
