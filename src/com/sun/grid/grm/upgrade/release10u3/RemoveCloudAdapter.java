/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2009 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/


package com.sun.grid.grm.upgrade.release10u3;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.bootstrap.PathUtil;
import com.sun.grid.grm.config.common.Component;
import com.sun.grid.grm.config.common.GlobalConfig;
import com.sun.grid.grm.config.common.JvmConfig;
import com.sun.grid.grm.ui.AbstractLocalCommand;
import com.sun.grid.grm.ui.Result;
import com.sun.grid.grm.ui.component.GetGlobalConfigurationCommand;
import com.sun.grid.grm.ui.component.RemoveComponentWithConfigurationCommand;
import com.sun.grid.grm.ui.impl.CommandResult;
import com.sun.grid.grm.upgrade.LocalMacroCommand;
import com.sun.grid.grm.upgrade.UpgradeStep;
import com.sun.grid.grm.util.Hostname;
import com.sun.grid.grm.util.I18NManager;
import com.sun.grid.grm.util.Platform;
import java.io.File;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Logger;

/**
 * 1.0u3 cloud adapter is not supported. Service with spooled dir are removed from master host.
 */
public class RemoveCloudAdapter extends AbstractLocalCommand<Void> implements UpgradeStep {
    private static final String classname = "com.sun.grid.grm.service.impl.cloud.CloudServiceImpl";
    private static final String BUNDLE="com.sun.grid.grm.upgrade.release10u3.messages";
    private final LocalMacroCommand cmds = new LocalMacroCommand();
    private final static Logger log = Logger.getLogger(RemoveCloudAdapter.class.getName());
    public Result<Void> execute(ExecutionEnv env) throws GrmException {
        log.entering(RemoveCloudAdapter.class.getName(), "execute", env);
        List<String> clouds;
        GlobalConfig gc;
        File tmp;
        Hostname localhost = Hostname.getLocalHost();
        //only on master
        if (!localhost.equals(env.getCSHost())) {
            //we are on managed, this is master host upgrade
            //If some user installed cloud on managed he should clean it up manually
            //it is not supported 
            return new CommandResult<Void>();
        }
        try {
            //remove from configs and remove the spool dir
            gc = env.getCommandService().executeLocally(new GetGlobalConfigurationCommand()).getReturnValue();
            //search for cloud adapter component and remove it
            clouds = getCloudComponentNames(gc);
            /***
             * BackupDirectoryCommand is moving source to target and this operation is overwritting the tmp location.
             * Therefore we need to create new tmp directory for each "moved" directory
             */
            for (String name : clouds) {
                File spool = PathUtil.getSpoolDirForComponent(env, name);
                File compTmp = PathUtil.getTmpDirForComponent(env, name);
                if (spool.isDirectory()) {
                    tmp = Platform.getPlatform().createTmpDir("upgrade", "spool");
                    BackupDirectoryCommand compSpool = new BackupDirectoryCommand(spool, tmp, true);
                    cmds.addCommand(compSpool);
                }
                if (compTmp.isDirectory()) {
                    tmp = Platform.getPlatform().createTmpDir("upgrade", "tmp");
                    BackupDirectoryCommand compTmpDir = new BackupDirectoryCommand(compTmp, tmp, true);
                    cmds.addCommand(compTmpDir);
                }
                RemoveComponentWithConfigurationCommand cmd = new RemoveComponentWithConfigurationCommand(name, localhost.getHostname());
                cmds.addCommand(cmd);
            }
            env.getCommandService().executeLocally(cmds);
        } catch (GrmException ex) {
            this.undo(env);
            throw ex;
        }
        Result<Void> ret = CommandResult.VOID_RESULT;
        log.exiting(RemoveCloudAdapter.class.getName(), "execute", ret);
        return ret;
    }

    public String getName() {
        return I18NManager.formatMessage("RemoveCloudAdapter.desc", BUNDLE);
    }

    @Override
    public void undo(ExecutionEnv env) {
        log.entering(RemoveCloudAdapter.class.getName(), "undo", env);
        cmds.undo(env);
        log.exiting(RemoveCloudAdapter.class.getName(), "undo", env);
    }
    /**
     * There should be one but u never know
     * @param gc GlobalConfig
     * @return list of components with defined CloudAdapterImpl as classname
     */
    private List<String> getCloudComponentNames(GlobalConfig gc) {
        log.entering(RemoveCloudAdapter.class.getName(), "getCloudComponentNames", gc);
        List<String> ret = new LinkedList<String>();
        for (JvmConfig jvm : gc.getJvm()) {
            for (Component c : jvm.getComponent()) {
                if (classname.equals(c.getClassname())) {
                    ret.add(c.getName());
                }
            }
        }
        log.exiting(RemoveCloudAdapter.class.getName(), "getCloudComponentNames", ret);
        return ret;
    }
}
