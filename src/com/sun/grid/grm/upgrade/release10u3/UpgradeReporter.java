/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2009 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/


package com.sun.grid.grm.upgrade.release10u3;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.bootstrap.PathUtil;
import com.sun.grid.grm.config.common.GlobalConfig;
import com.sun.grid.grm.ui.AbstractLocalCommand;
import com.sun.grid.grm.ui.Result;
import com.sun.grid.grm.ui.component.GetGlobalConfigurationCommand;
import com.sun.grid.grm.ui.impl.CommandResult;
import com.sun.grid.grm.upgrade.UpgradeStep;
import com.sun.grid.grm.util.Hostname;
import com.sun.grid.grm.util.I18NManager;
import com.sun.grid.grm.util.Platform;
import java.io.File;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Logger;

/**
 * Basic upgrade of reporter component. Rename all history files so startup of
 * upgraded system will write to new files. Old files will be still accessible manually -
 * from spool directory.
 */
public class UpgradeReporter extends AbstractLocalCommand<Void> implements UpgradeStep{

    private final List<BackupDirectoryCommand> undos = new LinkedList<BackupDirectoryCommand>();
    private static String BUNDLE = "com.sun.grid.grm.upgrade.release10u3.messages";
    private final Logger log = Logger.getLogger(UpgradeReporter.class.getName(), BUNDLE);
    private final String classname = "com.sun.grid.grm.reporting.impl.ReporterImpl";

    public Result<Void> execute(ExecutionEnv env) throws GrmException {
        //we need global config
        boolean master = Hostname.getLocalHost().equals(env.getCSHost());

        GlobalConfig global;
        GetGlobalConfigurationCommand globalCmd = new GetGlobalConfigurationCommand();
        try {
            if (master) {
                //read global from context
                global = env.getCommandService().executeLocally(globalCmd).getReturnValue();
            } else {
                //contact CS to get config
                global = env.getCommandService().execute(globalCmd).getReturnValue();
            }
            for (ComponentOwner c : getReporters(global)) {
                File spool = PathUtil.getSpoolDirForComponent(env, c.getName());
                if (spool.exists()) {
                    BackupDirectoryCommand undoreporter = new BackupDirectoryCommand(spool, Platform.getPlatform().createTmpDir("upgrade", null), false);
                    undoreporter.execute(env);
                    undos.add(undoreporter);
                    //we can rename files
                    for (File f : spool.listFiles()) {
                        if (f.isFile()) {
                            if (!f.renameTo(generateNewFileName(f))) {
                                throw new GrmException("UpgradeReporter.renamefailed", BUNDLE, f.getAbsolutePath());
                            }
                        }
                    }
                    Util.resetComponentFiles(env, c.getName(), c.getOwner());
                }
            }
        } catch (GrmException ex) {
            this.undo(env);
            throw ex;
        }
        return new CommandResult<Void>();
    }

    public String getName() {
        return I18NManager.formatMessage("UpgradeReporter.desc", BUNDLE);
    }
    /**
     * Undo is restoring initial content of reporter directory
     * @param env ExecutionEnv
     */
    @Override
    public void undo(ExecutionEnv env) {
        for (BackupDirectoryCommand c : undos) {
            c.undo(env);
        }
        undos.clear();
    }
    private List<ComponentOwner> getReporters(GlobalConfig gc) {
        List<ComponentOwner> ret = new LinkedList<ComponentOwner>();

        for (ComponentOwner co : Util.getSingletonComponentsWithOwners(gc)) {
            if (co.getClassname().equals(classname)) {
                ret.add(co);
            }
        }
       return ret;
    }
    /**
     * 10 tries and then failure is reported
     * @param old - old file name
     * @return File object representing new unique filename
     * @throws com.sun.grid.grm.GrmException - if cannot generate unique filename
     */
    private File generateNewFileName(File old) throws GrmException {
        File ret;
        for (int i=0; i<10; i++) {
            ret = new File(new File(old.getParent()), old.getName()+"."+i+".old_10u3");
            if (!ret.exists()) {
                return ret;
            }
        }
        throw new GrmException("UpgradeReporter.error.cannotgenerateuniquefile", BUNDLE, old.getAbsolutePath());
    }

}
