/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2009 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/


package com.sun.grid.grm.upgrade.release10u3;

import com.sun.grid.grm.service.impl.ge.HostFileStore;
import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.bootstrap.PathUtil;
import com.sun.grid.grm.config.common.GlobalConfig;
import com.sun.grid.grm.resource.AbstractResourceType;
import com.sun.grid.grm.resource.DefaultResourceFactory;
import com.sun.grid.grm.resource.HostResourceType;
import com.sun.grid.grm.resource.Resource;
import com.sun.grid.grm.resource.ResourceType;
import com.sun.grid.grm.resource.impl.FileResourceStore;
import com.sun.grid.grm.resource.impl.FixedResourceIdFactory;
import com.sun.grid.grm.resource.impl.ResourceImpl;
import com.sun.grid.grm.service.impl.ge.Host;
import com.sun.grid.grm.service.impl.ge.HostFactory;
import com.sun.grid.grm.ui.AbstractLocalCommand;
import com.sun.grid.grm.ui.Result;
import com.sun.grid.grm.ui.component.GetGlobalConfigurationCommand;
import com.sun.grid.grm.ui.component.GetNextResourceIdCommand;
import com.sun.grid.grm.ui.impl.CommandResult;
import com.sun.grid.grm.upgrade.UpgradeStep;
import com.sun.grid.grm.util.Hostname;
import com.sun.grid.grm.util.I18NManager;
import com.sun.grid.grm.util.Platform;
import java.io.File;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Logger;


/**
 *
 * Command that handles resources spooled by 1.0u3. During upgrade all resources owned by
 * service/resource provider are considered as 1.0u3 system resources
 */
public class UpgradeResourceFiles extends AbstractLocalCommand<Void> implements UpgradeStep {

    private final List<BackupDirectoryCommand> undos = new LinkedList<BackupDirectoryCommand>();
    private static String BUNDLE = "com.sun.grid.grm.upgrade.release10u3.messages";
    private final Logger log = Logger.getLogger(UpgradeResourceFiles.class.getName(), BUNDLE);


    public Result<Void> execute(ExecutionEnv env) throws GrmException {

        boolean resetComp = false;
        boolean master = Hostname.getLocalHost().equals(env.getCSHost());
        ResourceType hostResourceType = DefaultResourceFactory.getResourceType(HostResourceType.HOST_TYPE);
        List<Resource> old;
        GlobalConfig global;
        GetGlobalConfigurationCommand globalCmd = new GetGlobalConfigurationCommand();
        try {
            if (master) {
                //read global from context
                global = env.getCommandService().executeLocally(globalCmd).getReturnValue();
            } else {
                //contact CS to get config
                global = env.getCommandService().execute(globalCmd).getReturnValue();
            }
            //GetComponents with owners for current host
            List<ComponentOwner> componentOwners = Util.getSingletonComponentsWithOwners(global);
            //resource_provider needs special treatment,
            //it has different spool and wee need to clear servicecaching proxies spools

            //we have candidates, now go over spools
            for (ComponentOwner c : componentOwners) {
                File spool;
                if (c.getClassname().equals("com.sun.grid.grm.resource.impl.ResourceProviderImpl")) {
                    //resource_provider processing
                    spool = PathUtil.getRPSpoolPath(env);
                    //for resource provider the SCP spools should be cleared
                    if (spool.exists()) {
                        for (File f : spool.listFiles()) {
                            //we had resource_provider hardcoded in PathUtil for 1.0u3
                            if (f.isDirectory() && !f.getName().equals(PathUtil.PATH_RP_SPOOL)) {
                                //save directory for undo
                                BackupDirectoryCommand undoscp = new BackupDirectoryCommand(f, Platform.getPlatform().createTmpDir("upgrade", f.getName()), true);
                                undoscp.execute(env);
                                undos.add(undoscp);
                            }
                        }
                    }
                    //now process resource_provider resources
                    spool = PathUtil.getRPSpoolDirForComponent(env, c.getName());
                } else if (c.getClassname().equals("com.sun.grid.grm.service.impl.ge.GEServiceContainer")) {
                    //GE adapter is spooling Host Objects
                    spool = PathUtil.getSpoolDirForComponent(env, c.getName());
                    if (spool.isDirectory()) {                      
                            BackupDirectoryCommand undospool = new BackupDirectoryCommand(spool, Platform.getPlatform().createTmpDir("upgrade", null), false);
                            undospool.execute(env);
                            undos.add(undospool);
                            //Read the store
                            HostFileStore store = new HostFileStore(spool);
                            Set<Host> hosts = store.load();

                            //Remove the spooled file (it has to succeed otherwise resource could be duplicated
                            for (Host host : hosts) {
                                if (!store.delete(host)) {
                                    throw new GrmException("UpgradeResourceFiles.error.faileddeletehostfile", BUNDLE, host.getName().getHostname());
                                }
                                Resource r = host.getResource();
                                long resourceId;
                                //Retrieve the resource Id
                                if (master) {
                                    //cs not running, execute locally command
                                    resourceId = env.getCommandService().executeLocally(new GetNextResourceIdCommand()).getReturnValue();
                                    //reset of files in CS is done globally
                                } else {
                                    //cs running simply ask for new resourceId
                                    resourceId = env.getCommandService().execute(new GetNextResourceIdCommand()).getReturnValue();
                                }
                                FixedResourceIdFactory factory = new FixedResourceIdFactory(resourceId);
                                Map<String,Object> props = new HashMap<String,Object>(r.getProperties());

                                // Set the static flag only if user set the resource static
                                // In all other cases the Host object will set the static
                                // flag automatically.
                                if(!isUserSetStatic(host)) {
                                    props.remove(AbstractResourceType.STATIC.getName());
                                }
                                
                                //All resources from 1.0u3 should be Host_Type
                                Resource n = new ResourceImpl(hostResourceType, factory, props);
                                //Create new instance of Host
                                Host newHost = HostFactory.newInstance(n);
                                newHost.setHostState(host.getHostState(), I18NManager.formatMessage("UpgradeResourcesFiles.upgraded", BUNDLE));
                                //Store new Host Object
                                store.write(newHost);
                                //mark the reset of file owners
                                resetComp = true;
                            }
                            if (resetComp) {
                                Util.resetComponentFiles(env, c.getName(), c.getOwner());
                                resetComp = false;
                            }
                            //This components spooled data is upgraded. Go to next one.
                            continue;
                    }
                } else {
                    spool = PathUtil.getSpoolDirForComponent(env, c.getName());
                }
                if (spool.isDirectory()) {
                    BackupDirectoryCommand undospool = new BackupDirectoryCommand(spool, Platform.getPlatform().createTmpDir("upgrade", null), false);
                    undospool.execute(env);
                    undos.add(undospool);
                    //spool exists go over resource files
                    FileResourceStore store = new FileResourceStore(spool);
                    //we are able to read 1.0u3 resources
                    store.loadResources();
                    old = store.getResources();
                    store.clear();
                    for (Resource r : old) {
                        //recreate new resource, set unbound_name and create new ResourceId
                        long resourceId;
                        if (master) {
                            //cs not running, execute locally command
                            resourceId = env.getCommandService().executeLocally(new GetNextResourceIdCommand()).getReturnValue();
                        //reset of files in CS is done globally
                        } else {
                            //cs running simply ask for new resourceId
                            resourceId = env.getCommandService().execute(new GetNextResourceIdCommand()).getReturnValue();
                        }

                        FixedResourceIdFactory factory = new FixedResourceIdFactory(resourceId);
                        //All resources from 1.0u3 should be Host_Type
                        Resource n = new ResourceImpl(hostResourceType, factory, r.getProperties());
                        //set the same state
                        n.setState(r.getState());
                        //refill store with resource and persist
                        store.add(n);
                        //change owner so resource files are owned by real owner
                        resetComp = true;
                    }
                    if (resetComp) {
                        Util.resetComponentFiles(env, c.getName(), c.getOwner());
                        resetComp = false;
                    }
                }
            }
        } catch (GrmException ex) {
            this.undo(env);
            throw ex;
        }
        return new CommandResult<Void>();
    }

    public String getName() {
        return I18NManager.formatMessage("UpgradeResourcesFiles.desc", BUNDLE);
    }

    @Override
    public void undo(ExecutionEnv env) {
        for (BackupDirectoryCommand c : undos) {
            c.undo(env);
        }
        undos.clear();
    }

    private static boolean isUserSetStatic(Host host) {
        try {
            Field f = host.getClass().getDeclaredField("userSetStaticFlag");
            f.setAccessible(true);
            try {
                return f.getBoolean(host);
            } finally {
                f.setAccessible(false);
            }
        } catch(Exception ex) {
            // Should not happen
            return false;
        }
    }
}
