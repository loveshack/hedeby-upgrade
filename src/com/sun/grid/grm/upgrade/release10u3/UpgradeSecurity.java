/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2009 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/


package com.sun.grid.grm.upgrade.release10u3;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.bootstrap.PreferencesType;
import com.sun.grid.grm.bootstrap.PreferencesUtil;
import com.sun.grid.grm.security.SecurityPathUtil;
import com.sun.grid.grm.security.ui.CreateUserKeystoreCommand;
import com.sun.grid.grm.ui.AbstractLocalCommand;
import com.sun.grid.grm.ui.Command;
import com.sun.grid.grm.ui.Result;
import com.sun.grid.grm.ui.impl.CommandResult;
import com.sun.grid.grm.upgrade.UpgradeStep;
import com.sun.grid.grm.util.Hostname;
import com.sun.grid.grm.util.I18NManager;
import com.sun.grid.grm.util.Platform;

/**
 * Command to upgrade security (issue 688 changes)
 * Root keystores not created for system preferences systems on managed host
 */
public class UpgradeSecurity extends AbstractLocalCommand<Void> implements UpgradeStep{
    private static final String BUNDLE="com.sun.grid.grm.upgrade.release10u3.messages";
    private Command undo = null;
    public Result<Void> execute(ExecutionEnv env) throws GrmException {
        boolean exists = false;
        //check if really sth needs to be done
        if (!env.getCSHost().equals(Hostname.getLocalHost()) && Platform.getPlatform().isSuperUser()) {
            for (String s : PreferencesUtil.getSystemNames(PreferencesType.SYSTEM)) {
                if (s.equals(env.getSystemName())) {
                    //check if keystore file exists, user could create it manually
                    if (SecurityPathUtil.getUserKeystoreFile(env, "root").exists()) {
                        exists = true;
                        break;
                    }
                }
            }
            if (exists) {
                //no need for action, 688 was present just in system preferences systems
                return new CommandResult<Void>();
            }
            //Execute security commands responsible for this
            CreateUserKeystoreCommand cmd = new CreateUserKeystoreCommand("root");
            env.getCommandService().execute(cmd);
            undo = cmd;
        }
        //trigger command to create keystore for root
        return new CommandResult<Void>();
    }

    public String getName() {
        return I18NManager.formatMessage("UpgradeSecurity.desc", BUNDLE);
    }
    @Override
    public void undo(ExecutionEnv env) {
        //remove root keystore if it was created
        if (undo != null) {
            undo.undo(env);
            undo = null;
        }
    }
    
}
