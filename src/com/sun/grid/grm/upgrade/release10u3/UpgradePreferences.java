/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2009 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/


package com.sun.grid.grm.upgrade.release10u3;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.bootstrap.PreferencesType;
import com.sun.grid.grm.bootstrap.PreferencesUtil;
import com.sun.grid.grm.ui.AbstractLocalCommand;
import com.sun.grid.grm.ui.Result;
import com.sun.grid.grm.ui.impl.CommandResult;
import com.sun.grid.grm.upgrade.UpgradeStep;
import com.sun.grid.grm.util.I18NManager;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;
import java.util.prefs.BackingStoreException;


/**
 * Command for upgrading preferences for systems 1.0u3
 * Removal of version key and setting preferences ID
 */
public class UpgradePreferences extends AbstractLocalCommand<Void> implements UpgradeStep {

    private static final String BUNDLE = "com.sun.grid.grm.upgrade.release10u3.messages";    
    private final List<UndoHolder> undos = new LinkedList<UndoHolder>();
    private final Logger log = Logger.getLogger(UpgradePreferences.class.getName(), BUNDLE);

    public String getName() {
        return I18NManager.formatMessage("UpgradePreferences.desc", BUNDLE);
    }

    public Result<Void> execute(ExecutionEnv env) throws GrmException {
        List<PreferencesType> prefs = new LinkedList<PreferencesType>();
        //We upgrade all readable preferences for given system name
        for (String name : PreferencesUtil.getSystemNames(PreferencesType.SYSTEM)) {
            if (name.equals(env.getSystemName())) {
                //system found in SYSTEM preferences
                prefs.add(PreferencesType.SYSTEM);
            }
        }

        for (String name : PreferencesUtil.getSystemNames(PreferencesType.USER)) {
            if (name.equals(env.getSystemName())) {
                //system found in user preferences
                prefs.add(PreferencesType.USER);
            }
        }
        //Prepare for undo
        for (PreferencesType p : prefs) {
            try {
                undos.add(new UndoHolder(p, PreferencesUtil.exportSystemPreferences(p, env.getSystemName())));
                PreferencesUtil.removeVersion(env.getSystemName(), p);
                PreferencesUtil.setPreferencesId(env.getSystemName(), p);
            } catch (IOException ex) {
                this.undo(env);
                throw new GrmException("UpgradePreferences.error.store", BUNDLE, ex, p.toString(), env.getSystemName());

            } catch (BackingStoreException ex) {
                this.undo(env);
                throw new GrmException("UpgradePreferences.error.store", BUNDLE, ex, p.toString(), env.getSystemName());
            }
        }
        //Upgrade completed
        return new CommandResult<Void>();
    }

    @Override
    public void undo(ExecutionEnv env) {
        for (UndoHolder u : undos) {
            try {
                PreferencesUtil.importSystemPreferences(u.getPrefs(), env.getSystemName(), u.getValue());
            } catch (Exception ex) {
                //log undo failure
                LogRecord lr = new LogRecord(Level.WARNING, "UpgradePreferences.undo.failed");
                lr.setParameters(new Object[]{u.getPrefs().toString(), env.getSystemName()});
                lr.setThrown(ex);
                log.log(lr);
            }
        }
    }

    /**
     * Helper class that allows easily keep data for undo
     */
    class UndoHolder {

        private final PreferencesType prefs;
        private byte[] value;

        public UndoHolder(PreferencesType prefs, byte[] value) {
            this.prefs = prefs;

            this.value = new byte[value.length];
            for (int i=0; i < value.length; i++) {
                this.value[i] = value[i];
            }
        }

        /**
         * @return the prefs
         */
        public PreferencesType getPrefs() {
            return prefs;
        }

        /**
         * @return the value
         */
        public byte[] getValue() {
            return value;
        }
    }
}

