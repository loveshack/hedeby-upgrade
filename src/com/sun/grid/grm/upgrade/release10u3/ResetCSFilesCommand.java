/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2009 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.upgrade.release10u3;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.bootstrap.BootstrapConstants;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.bootstrap.PathUtil;
import com.sun.grid.grm.config.common.GlobalConfig;
import com.sun.grid.grm.config.common.JvmConfig;
import com.sun.grid.grm.ui.AbstractLocalCommand;
import com.sun.grid.grm.ui.Result;
import com.sun.grid.grm.ui.component.GetGlobalConfigurationCommand;
import com.sun.grid.grm.ui.impl.CommandResult;
import com.sun.grid.grm.util.Platform;
import java.io.File;
import java.io.IOException;

/**
 * Command that resets the owner of CS files to the owner that is specified in global configuration
 */

public class ResetCSFilesCommand extends AbstractLocalCommand<Void>{
    private static final String BUNDLE="com.sun.grid.grm.upgrade.release10u3.messages";

    public Result<Void> execute(ExecutionEnv env) throws GrmException {
        File spool= PathUtil.getSpoolDirForComponent(env, BootstrapConstants.CS_ID);
        String owner = null;
        GlobalConfig global = env.getCommandService().executeLocally(new GetGlobalConfigurationCommand()).getReturnValue();

        for (JvmConfig j : global.getJvm()) {
            if (j.getName().equals(BootstrapConstants.CS_JVM)) {
                owner = j.getUser();
                break;
            }
        }
        if (owner == null) {
            throw new GrmException("ResetCSFilesCommand.error.csnotfound", BUNDLE);
        }
        
        try {
            Platform.getPlatform().chown(spool, owner, true);
        } catch (IOException ex) {
            throw new GrmException("ResetCSFilesCommand.error.changeowner", BUNDLE, ex,spool.getAbsolutePath(), owner);
        } catch (InterruptedException ex) {
            throw new GrmException("ResetCSFilesCommand.error.changeowner", BUNDLE, ex,spool.getAbsolutePath(), owner);
        }
        return CommandResult.VOID_RESULT;
    }

}
