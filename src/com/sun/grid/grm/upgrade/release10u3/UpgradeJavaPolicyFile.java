/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2009 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.upgrade.release10u3;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.bootstrap.PathUtil;
import com.sun.grid.grm.security.ui.CreatePolicyFileCommand;
import com.sun.grid.grm.ui.AbstractLocalCommand;
import com.sun.grid.grm.ui.Result;
import com.sun.grid.grm.ui.impl.CommandResult;
import com.sun.grid.grm.upgrade.UpgradeStep;
import com.sun.grid.grm.util.I18NManager;
import com.sun.grid.grm.util.Platform;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * The fix of issue 692 requires an upgrade of the java policy file. This upgrade
 * step performs this upgrade.
 *
 * It copies the java.policy.template from the dist directory to the java.policy file
 * in the local spool dir. The action itself is implemented in <tt>CreatePolicyFileCommand</tt>.
 *
 * Before copying the java.policy file a backup is made (filename java.policy.u3). This way
 * user changes to the original file are not lost.
 */
public class UpgradeJavaPolicyFile extends AbstractLocalCommand<Void> implements UpgradeStep {

    static final String BUNDLE = "com.sun.grid.grm.upgrade.release10u3.messages";
    private static final Logger log = Logger.getLogger(UpgradeJavaPolicyFile.class.getName(), BUNDLE);
    
    /**
     * Holds the backup file for undo
     */
    private File backupFile;
    
    /**
     * Performs an upgrade of the java.policy file by using the
     * <tt>CreatePolicyFileCommand</tt>
     * @param env  the execution env
     * @return void result
     * @throws com.sun.grid.grm.GrmException
     * @see CreatePolicyFileCommand
     */
    public Result<Void> execute(ExecutionEnv env) throws GrmException {
        log.entering(UpgradeJavaPolicyFile.class.getName(), "execute", env);

        File policyFile = PathUtil.getSecurityJavaPolicy(env);
        if (policyFile.exists()) {
            String owner;
            try {
                owner = Platform.getPlatform().getFileOwner(policyFile);
            } catch (IOException ex) {
                throw new GrmException("UpgradeJavaPolicyFile.ex.getOwner", ex.getLocalizedMessage(), BUNDLE, ex);
            }
            File tmpBackupFile = new File(policyFile.getParentFile(), policyFile.getName() + ".10u3");
            if (!policyFile.renameTo(tmpBackupFile)) {
                throw new GrmException("UpgradeJavaPolicyFile.ex.backup", BUNDLE, policyFile, tmpBackupFile);
            }
            this.backupFile = tmpBackupFile;
            // Undo of the CreatePolicyFileCommand is not necessary
            // If undo is called we delete the new policy file and rename the backup
            CreatePolicyFileCommand cmd = new CreatePolicyFileCommand(owner);
            cmd.execute(env);
        } else {
            throw new GrmException("UpgradeJavaPolicyFile.ex.noPolicyFile", BUNDLE, policyFile);
        }
        
        log.exiting(UpgradeJavaPolicyFile.class.getName(), "execute", CommandResult.VOID_RESULT);
        return CommandResult.VOID_RESULT;
    }

    /**
     * This undo method restores the original java.policy file by
     * renaming the backup file.
     * 
     * @param env the execution env
     */
    @Override
    public void undo(ExecutionEnv env) {
        log.entering(UpgradeJavaPolicyFile.class.getName(), "undo", env);
        if (backupFile != null) {
            File policyFile = PathUtil.getSecurityJavaPolicy(env);
            if (policyFile.exists() && !policyFile.delete()) {
                log.log(Level.WARNING, "UpgradeJavaPolicyFile.delete.failed", policyFile);
            }
            if (!backupFile.renameTo(policyFile)) {
                log.log(Level.WARNING, "UpgradeJavaPolicyFile.restore.failed", new Object [] { backupFile, policyFile });
            }
        }
        log.exiting(UpgradeJavaPolicyFile.class.getName(), "undo", env);
    }

    /**
     * Get the name of the upgrade step
     * @return the name of the upgrade step
     */
    public String getName() {
        return I18NManager.formatMessage("UpgradeJavaPolicyFile.desc", BUNDLE);
    }
}
